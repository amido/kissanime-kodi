
import re
import zlib
import socket
import select


REQ = 'GET /Anime/{anime_name} HTTP/1.1\r\nHost: kissanime.to\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36 Vivaldi/1.2.490.43\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate, sdch\r\nAccept-Language: en-US,en;q=0.8\r\nCookie: __cfduid=dca5ac90e34847a5740c78ffa120d49a41470083928; ASP.NET_SessionId=rssgzxejwrhvv1t4tebvm0tt; idtz=128.90.59.249-336906116; MarketGidStorage=%7B%220%22%3A%7B%22svspr%22%3A%22%22%2C%22svsds%22%3A13%2C%22TejndEEDj%22%3A%22MTQ3MDA4Mzk1NDI4MzYyNjk1MTIwMQ%3D%3D%22%7D%2C%22C62695%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470172027761%7D%2C%22C56024%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470172027693%7D%7D; __test; __atuvc=9%7C31; __PPU_SESSION_1_706755_false=1470198459216|1|1470198459217|1|1; cf_clearance=f934556dbf7c2704c72b5a0c61464e76079e03c1-1470399848-86400; noadvtday=0; nopopatall=1470399870; _gat=1; _ga=GA1.2.415448371.1470083929; bbl=5; BB_plg=pm\r\n\r\n'
REQ_FULL = 'GET {path} HTTP/1.1\r\nHost: kissanime.to\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36 Vivaldi/1.2.490.43\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate, sdch\r\nAccept-Language: en-US,en;q=0.8\r\nCookie: __cfduid=dca5ac90e34847a5740c78ffa120d49a41470083928; ASP.NET_SessionId=rssgzxejwrhvv1t4tebvm0tt; idtz=128.90.59.249-336906116; MarketGidStorage=%7B%220%22%3A%7B%22svspr%22%3A%22%22%2C%22svsds%22%3A13%2C%22TejndEEDj%22%3A%22MTQ3MDA4Mzk1NDI4MzYyNjk1MTIwMQ%3D%3D%22%7D%2C%22C62695%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470172027761%7D%2C%22C56024%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470172027693%7D%7D; __test; __atuvc=9%7C31; __PPU_SESSION_1_706755_false=1470198459216|1|1470198459217|1|1; cf_clearance=f934556dbf7c2704c72b5a0c61464e76079e03c1-1470399848-86400; noadvtday=0; nopopatall=1470399870; _gat=1; _ga=GA1.2.415448371.1470083929; bbl=5; BB_plg=pm\r\n\r\n'
SEARCH = 'POST /AdvanceSearch HTTP/1.1\r\nHost: kissanime.to\r\nConnection: keep-alive\r\nContent-Length: {content_length}\r\nCache-Control: max-age=0\r\nOrigin: http://kissanime.to\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36 Vivaldi/1.2.490.43\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nReferer: http://kissanime.to/AdvanceSearch\r\nAccept-Encoding: gzip, deflate\r\nAccept-Language: en-US,en;q=0.8\r\nCookie: __cfduid=dca5ac90e34847a5740c78ffa120d49a41470083928; ASP.NET_SessionId=rssgzxejwrhvv1t4tebvm0tt; idtz=128.90.59.249-336906116; __test; __PPU_SESSION_1_706755_false=1470198459216|1|1470198459217|1|1; cf_clearance=f934556dbf7c2704c72b5a0c61464e76079e03c1-1470399848-86400; MarketGidStorage=%7B%220%22%3A%7B%22svspr%22%3A%22%22%2C%22svsds%22%3A15%2C%22TejndEEDj%22%3A%22MTQ3MDA4Mzk1NDI4MzYyNjk1MTIwMQ%3D%3D%22%7D%2C%22C62695%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470413523015%7D%2C%22C56024%22%3A%7B%22page%22%3A1%2C%22time%22%3A1470413522979%7D%7D; __atuvc=11%7C31; _gat=1; noadvtday=0; nopopatall=1470416529; _ga=GA1.2.415448371.1470083929; bbl=3; BB_plg=pm\r\n\r\n'
SEARCH_PAYLOAD = 'animeName={search_str}&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&genres=0&status='
ADDR = ('kissanime.to', 80)

    
class TYPES:
    SERIES = 2
    EPISODE = 3


def parse_page(data):
    start = data.find('<table class="listing">')
    end = data[start:].find('</table>') + start
    episodes = data[start:end]
    links = parse_links(episodes)
    descriptions = parse_descriptions(episodes)
    return zip(links, descriptions)

def parse_href(data):
    matches = re.findall('<a\s+href="(.*?)">(.*?)</a>', data, re.S)
    matches = [(href_link.strip(), href_content.strip()) for href_link, href_content in matches]
    return matches
    
    
def parse_links(data):
    return re.findall('<a\s+href="(.*?)".*?>.*?</a>', data, re.S)
    

def parse_descriptions(data):
    desc_lst = re.findall('<a\s+href=".*?".*?>(.*?)</a>', data, re.S)
    return map(lambda x: x.strip(), desc_lst)
    
    
def send_kissanime(req):
    sock = socket.socket()
    sock.connect(ADDR)
    sock.sendall(req)
    return sock
    
    
def recv_kissanime(sock):
    sock.settimeout(5)
    data = ''
    
    try:
        while 1:
            data += sock.recv(4096)
    except socket.timeout:
        pass
    
    # expecting gzipped data
    header, payload, _ = data.split('\r\n\r\n')
    chunks_with_lengths = payload.split('\r\n')
    chunk_lengths = [chunks_with_lengths[i] for i in xrange(0, len(chunks_with_lengths), 2)]
    data_chunks = [chunks_with_lengths[i] for i in xrange(1, len(chunks_with_lengths) - 1, 2)]
    gzipped_data = ''.join(data_chunks)
    uncompressed_data = zlib.decompress(gzipped_data, zlib.MAX_WBITS + 16)
    return uncompressed_data
    
    
def get_episodes_list(path):
    req = REQ_FULL.format(path=path)
    sock = send_kissanime(req)
    data = recv_kissanime(sock)
    return parse_page(data)
    
    
def get_kissanime_url(path):
    req = REQ_FULL.format(path=path)
    sock = send_kissanime(req)
    data = recv_kissanime(sock)
    return data
    
    
def get_video_links(path):
    data = get_kissanime_url(path)
    return parse_video_links(data)

    
def parse_video_links(data):
    start = data.find('<select id="selectQuality">')
    end = data[start:].find('</select>') + start
    select_quality = data[start:end]
    options = parse_quality_options(select_quality)
    return options
    

def parse_quality_options(select_quality):
    video_links = re.findall('value="(.*?)"', select_quality)
    quality = re.findall('([0-9]{3,4}p)</option>', select_quality)
    return zip(video_links, quality)
    

def kissanime_search(search_str):
    search_str = search_str.replace(' ', '+')
    payload = SEARCH_PAYLOAD.format(search_str=search_str)
    search_req = SEARCH.format(content_length=len(payload)) + payload
    sock = send_kissanime(search_req)
    data = recv_kissanime(sock)
    return parse_page(data)
    
    
def kissanime_get_url_type(path):
    if path.endswith("/"):
        path = path[:-1]
    return path.count("/")
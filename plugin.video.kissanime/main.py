
import sys
import xbmc
import xbmcgui
import xbmcaddon
import xbmcplugin
from urlparse import parse_qsl
from resources.lib import kissanime


# Get the plugin url in plugin:// notation.
__url__ = sys.argv[0]
# Get the plugin handle as an integer number.
__handle__ = int(sys.argv[1])


def get_categories():
    return ['Search']
    
def get_videos(category):
    return [{'name': 'test', 'thumb': '', 'video': 'http://google.com', 'genre': 'anime'}]
    
def list_categories():
    list_item = xbmcgui.ListItem(label='Search')
    url = '{0}?action=search'.format(__url__)
    xbmcplugin.addDirectoryItem(__handle__, url, list_item, isFolder=True)
    xbmcplugin.endOfDirectory(__handle__)
    
def list_videos(path):
    """
    Create the list of playable videos in the Kodi interface.
    :param category: str
    :return: None
    """
    # Get the list of videos in the category.
    videos = kissanime.get_episodes_list(path)
    # Create a list for our items.
    listing = []
    # Iterate through videos.
    for video_path, desc in videos:
        # Create a list item with a text label and a thumbnail image.
        list_item = xbmcgui.ListItem(label=desc)
        # Set a fanart image for the list item.
        # Here we use the same image as the thumbnail for simplicity's sake.
        #list_item.setProperty('fanart_image', video['thumb'])
        # Set additional info for the list item.
        #list_item.setInfo('video', {'title': video['name'], 'genre': video['genre']})
        # Set 'IsPlayable' property to 'true'.
        # This is mandatory for playable items!
        #list_item.setProperty('IsPlayable', 'true')
        # Create a URL for the plugin recursive callback.
        # Example: plugin://plugin.video.example/?action=play&video=http://www.vidsplay.com/vids/crab.mp4
        url = '{0}?action=chooseQuality&path={1}'.format(__url__, video_path)
        # Add the list item to a virtual Kodi folder.
        # is_folder = False means that this item won't open any sub-list.
        is_folder = True
        # Add our item to the listing as a 3-element tuple.
        listing.append((url, list_item, is_folder))
    # Add our listing to Kodi.
    # Large lists and/or slower systems benefit from adding all items at once via addDirectoryItems
    # instead of adding one by ove via addDirectoryItem.
    xbmcplugin.addDirectoryItems(__handle__, listing, len(listing))
    # Add a sort method for the virtual folder items (alphabetically, ignore articles)
    xbmcplugin.addSortMethod(__handle__, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
    # Finish creating a virtual folder.
    xbmcplugin.endOfDirectory(__handle__)
    

def choose_quality(path):
    # Get the list of videos in the category.
    videos = kissanime.get_video_links(path)
    # Create a list for our items.
    listing = []
    # Iterate through videos.
    for video_path, desc in videos:
        # Create a list item with a text label and a thumbnail image.
        list_item = xbmcgui.ListItem(label=desc)
        # Set a fanart image for the list item.
        # Here we use the same image as the thumbnail for simplicity's sake.
        #list_item.setProperty('fanart_image', video['thumb'])
        # Set additional info for the list item.
        #list_item.setInfo('video', {'title': video['name'], 'genre': video['genre']})
        # Set 'IsPlayable' property to 'true'.
        # This is mandatory for playable items!
        #list_item.setProperty('IsPlayable', 'true')
        # Create a URL for the plugin recursive callback.
        # Example: plugin://plugin.video.example/?action=play&video=http://www.vidsplay.com/vids/crab.mp4
        url = '{0}?action=play&path={1}'.format(__url__, video_path)
        # Add the list item to a virtual Kodi folder.
        # is_folder = False means that this item won't open any sub-list.
        is_folder = False
        # Add our item to the listing as a 3-element tuple.
        listing.append((url, list_item, is_folder))
    # Add our listing to Kodi.
    # Large lists and/or slower systems benefit from adding all items at once via addDirectoryItems
    # instead of adding one by ove via addDirectoryItem.
    xbmcplugin.addDirectoryItems(__handle__, listing, len(listing))
    # Add a sort method for the virtual folder items (alphabetically, ignore articles)
    xbmcplugin.addSortMethod(__handle__, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
    # Finish creating a virtual folder.
    xbmcplugin.endOfDirectory(__handle__)
    
    
def play_video(url):
    """
    Play a video by the provided url.
    :param url: str
    :return: None
    """
    url = url.decode('base64')
    # Create a playable item with a path to play.
    play_item = xbmcgui.ListItem(path=url)
    play_item.setProperty('Video', 'true')
    play_item.setProperty('IsPlayable', 'true')
    # Pass the item to the Kodi player.
    #xbmcplugin.setResolvedUrl(__handle__, True, listitem=play_item)
    player = xbmc.Player()
    player.play(url, play_item)
    xbmc.sleep(10000)
    
    
def do_search():
    t = xbmcaddon.Addon().getLocalizedString(30231).encode('utf-8')
    k = xbmc.Keyboard('', t)
    k.doModal()
    query = k.getText() if k.isConfirmed() else None
    if query:
        series_list = kissanime.kissanime_search(query)
        for serie_url, serie_desc in series_list:
            if kissanime.kissanime_get_url_type(serie_url) != kissanime.TYPES.SERIES:
                continue
                
            list_item = xbmcgui.ListItem(label=serie_desc)
            url = '{0}?action=listing&name={1}'.format(__url__, serie_url)
            xbmcplugin.addDirectoryItem(__handle__, url, list_item, isFolder=True)
        xbmcplugin.endOfDirectory(__handle__)
        
    
    
def router(paramstring):
    """
    Router function that calls other functions
    depending on the provided paramstring
    :param paramstring:
    :return:
    """
    # Parse a URL-encoded paramstring to the dictionary of
    # {<parameter>: <value>} elements
    params = dict(parse_qsl(paramstring[1:]))
    # Check the parameters passed to the plugin
    if params:
        if params['action'] == 'listing':
            # Display the list of videos in a provided category.
            list_videos(params['name'])
        elif params['action'] == 'chooseQuality':
            choose_quality(params['path'])
        elif params['action'] == 'play':
            # Play a video from a provided URL.
            play_video(params['path'])
        elif params['action'] == 'search':
            do_search()
    else:
        # If the plugin is called from Kodi UI without any parameters,
        # display the list of video categories
        list_categories()
        
if __name__ == '__main__':
    # Call the router function and pass the plugin call parameters to it.
    router(sys.argv[2])